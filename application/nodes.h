#ifndef NODES_H
#define NODES_H

#include <gmCoreModule>
#include <gmTriangleSystemModule>

class Nodes
{
public:
    Nodes();
    Nodes(GMlib::TSVertex<float>& vt);
    void setZ(float z);// set coordinate z
    bool isThis(GMlib::TSVertex<float>* vt);
    GMlib::TSEdge<float>* isNeighbor(Nodes& n);// return the edge from this vertex to neighour vertex
    GMlib::Array<GMlib::TSTriangle<float>* > getTriangles();// return array of triangle around the vertices
private:
    GMlib::TSVertex<float>* _vt;

};

#endif // NODES_H
