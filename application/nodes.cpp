#include "nodes.h"

Nodes::Nodes()
{
    _vt = nullptr;
}


Nodes::Nodes(GMlib::TSVertex<float> &vt)
{
    _vt = &vt;
}

void Nodes::setZ(float z)
{
    _vt->setZ(z);
}

GMlib::TSEdge<float>* Nodes::isNeighbor(Nodes& n)// checking for neighbor nodes
{
    GMlib::Array<GMlib::TSEdge<float>* > edge = _vt->getEdges();
    for (int i = 0; i < edge.size(); i++)
    {
        if(n.isThis(edge[i]->getOtherVertex(*_vt)))
            return edge[i];
    }

    return NULL;
}

GMlib::Array<GMlib::TSTriangle<float>* > Nodes::getTriangles()
{
    return _vt->getTriangles();
}

bool Nodes::isThis(GMlib::TSVertex<float>* vt)
{
    return  vt == _vt;
}
