#include "femsolver.h"
#include"nodes.h"
#include <gmCoreModule>
using namespace GMlib;

FemSolver::FemSolver(): GMlib::TriangleFacets<float>()// constructor
{
    numberOfBoundaryNodes = 0;
    update = 0;
    maxInterval = 1.0f; // height of mesh
    up = true;
    start = false;

}

FemSolver::FemSolver(const GMlib::ArrayLX<GMlib::TSVertex<float> > & v): GMlib::TriangleFacets<float>(v)// constructor
{
    numberOfBoundaryNodes = 0;
    update = 0;
    maxInterval = 2.0f;
    up = true;
    start = false;
}

void FemSolver::doComputation()
{
    makeNodes();
    makeStifnessMatrix();
    makeLoadVector();
    startSimulation();
}

// Makeing Nodes....................................................................................................

void FemSolver::makeNodes()
{
    for(int i = 0; i < this->getSize(); i++)
        if (!(*this)[i].boundary() )// if point is nto on boundary
            node += Nodes((*this)[i]); // add this point to array
}

// Stifness Matrix......................................................................................................

void FemSolver::makeStifnessMatrix()
{
    A.setDim(node.getSize(),node.getSize());// set dimension for new matrix

    for(int i = 0; i < node.getSize(); i++)
    {
        for(int j = 0; j < node.getSize(); j++)
        {
            GMlib::TSEdge<float> * edge = node[i].isNeighbor(node[j]);// checking nodes
            if (edge !=nullptr)
            {

                GMlib::Vector<GMlib::Vector<float,2>,3> d = findVector(edge);// d1, d2, d3

                float dd = (1/(d[0] * d[0]));

                float dh1 = dd * (d[1] * d[0]);// length
                float dh2 = dd * (d[2] * d[0]);

                float area1 = std::abs(d[1] ^ d[0]);// area of paralleogram make appropiate vector
                float area2 = std::abs(d[2] ^ d[0]);

                float  h1 = dd * area1 * area1;
                float  h2 = dd * area2 * area2;

                A[i][j] = A[j][i] = (((dh1*(1-dh1))/h1)-dd)*(area1/2) + (((dh2*(1-dh2))/h2)-dd)*(area2/2);

            }
            else
                A[i][j] = A[j][i] = 0.0;// filling with zeros

        }
// calculation of all triangels............................................................................................

        GMlib::Array<GMlib::TSTriangle<float>* > triangleArray = node[i].getTriangles();

        for (int k = 0; k < triangleArray.size(); k++)
        {
            GMlib::Vector<GMlib::Vector<float,2>,3> d = findVector(node[i], triangleArray[k]);
            A[i][i] += (d[2]*d[2]) / std::abs(2*(d[0] ^ d[1]));

        }
    }
    Ainvert = A.invert();
}

// find points and calculation or vector defination...........................................................................

GMlib::Vector<GMlib::Vector<float,2>,3> FemSolver::findVector(GMlib::TSEdge<float>* edg)

// find vector implement......................................................................................................
{
    GMlib::Vector<GMlib::Vector<float,2>,3> d;// find vector d1, d2, d3

    GMlib::Array<GMlib::TSTriangle<float>*> triangle = edg->getTriangle();
    GMlib::Array<GMlib::TSVertex<float>*> t1 = triangle[0]->getVertices();
    GMlib::Array<GMlib::TSVertex<float>*> t2 = triangle[1]->getVertices();

    GMlib::Point<float,2> p0 = edg->getFirstVertex()->getParameter();
    GMlib::Point<float,2> p1 = edg->getLastVertex()->getParameter();
    GMlib::Point<float,2> p2,p3;

    for(int i = 0; i < 3; i++)
    {
        if((t1[i] != edg->getFirstVertex()) && (t1[i] != edg->getLastVertex()))
            p2 = t1[i]->getParameter();


        if((t2[i] != edg->getFirstVertex()) && (t2[i] != edg->getLastVertex()))
            p3 = t2[i]->getParameter();
    }

    d[0] = p1 - p0;
    d[1] = p2 - p0;
    d[2] = p3 - p0;
    return d;

}

// makeing load vector.......................................................................................................

void FemSolver::makeLoadVector()
{
    B.setDim(node.size());
    for (int i = 0; i < node.size();i++)
    {
        GMlib::Array<GMlib::TSTriangle<float>*> tri = node[i].getTriangles();

        float volume = 0.0f;
        for(int j = 0; j < tri.size(); j++)
        {
            volume += tri[j]->getArea2D()/3;// calculating the volume of praymid
        }
        B[i] = volume;

    }
    //std::cout<< " load vector"<< B << std::endl;
}

// finding diagonal elements................................................................................................

GMlib::Vector<GMlib::Vector<float,2>,3> FemSolver::findVector(Nodes& node, GMlib::TSTriangle<float>* triangle)
{

    GMlib::Array<GMlib::TSVertex<float>*> vertices = triangle->getVertices();
    GMlib::Point<float,2> p0,p1,p2;

    if(node.isThis(vertices[1]))
    {
        std::swap(vertices[1],vertices[0]);// checking for triaagle vertices
        std::swap(vertices[1],vertices[2]);// checking for triaagle vertices
    }

    if(node.isThis(vertices[2]))
    {
        std::swap(vertices[0],vertices[2]);
        std::swap(vertices[2],vertices[1]);
    }
    GMlib::Vector<GMlib::Vector<float,2>,3> d;

    p0 = vertices[0]->getPos();
    p1 = vertices[1]->getPos();
    p2 = vertices[2]->getPos();

    d[0] = p1 - p0;// vector of triangles
    d[1] = p2 - p0;
    d[2] = p2 - p1;
    return d;

}

// calculating for height and load vector....................................................................................

void FemSolver::doCalculation(float x){

    GMlib::DVector<float> c;
    c.setDim(B.getDim());
    c = Ainvert * (x * B);

    for (int i = 0; i < node.getSize(); i++ ){
        node[i].setZ(c[i]);
    }
}
// Simulation of cricular mesh...............................................................................................

void FemSolver::startSimulation()
{
    start = true;
}

void FemSolver::localSimulate(double dt){

    if (start){
        if(up)
            update += dt/2;
        else
            update -= dt/2;

        if(update > maxInterval )
            up = false;

        if(update < -maxInterval)
            up = true;

        this->doCalculation(update);
        this->replot();
    }
}
