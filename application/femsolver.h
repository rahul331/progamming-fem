#ifndef FEMSOLVER_H
#define FEMSOLVER_H
#include <gmTriangleSystemModule>
#include <gmCoreModule>
#include "nodes.h"


class FemSolver:public GMlib::TriangleFacets<float>
{

public:
    FemSolver();
    FemSolver(const GMlib::ArrayLX<GMlib::TSVertex<float> > & v);

    void doComputation();
    void makeNodes();
    void makeStifnessMatrix();
    void makeLoadVector();
    void startSimulation();
    void doCalculation(float x); // calcualting x by invert of A multiply by B


private:

    GMlib::ArrayLX<Nodes> node;// array of nodes
    GMlib::DMatrix<float> A; // stifness Matrix
    GMlib::DVector<float> B; // laod vector
    GMlib::DMatrix<float> Ainvert;// Invert Matrix

    int numberOfBoundaryNodes;
    float update;
    float maxInterval;
    bool up;
    bool start;


    GMlib::Vector<GMlib::Vector<float,2>,3> findVector(GMlib::TSEdge<float>* edg);
    GMlib::Vector<GMlib::Vector<float,2>,3> findVector(Nodes& node, GMlib::TSTriangle<float>* triangle);

protected:

    void localSimulate(double dt);// function for movement

};

#endif // FEMSOLVER_H
